#AUDIMA OKTASENA

<div style="text-align:center">
<h2> ENDPOINT </h2>
<ul>
<li> http://localhost:3006/</li>
<li> http://localhost:3006/add</li>
<li> http://localhost:3006/edit/:id</li>
<li> http://localhost:3006/update/:id</li>
<li> http://localhost:3006/delete/:id</li>
</ul>

<p>get "/" = untuk mendapatkan semua data</p>
<p>post "/add" = untuk menambahkan sebuah data</p>
<p>get "/edit/:id" = untuk melakukan edit data</p>
<p>post "/update/:id" = untuk melakukan pengaupdatean data</p>
<p>get "/delete/:id" = untuk melakukan penghapusan data</p>

<h2> ENDPOINT POSTMAN </h2>
<p>get "/" = untuk mendapatkan semua data</p>
<p>get "/:id" = untuk mendapatkan data berdasarkan id</p>
<p>post "/" = untuk melakukan penambahan data</p>
<p>put "/:id" = untuk melakukan pengupdatean data</p>
<p>delete "/:id" = untuk melakukan penghapusan data berdasarkan id</p>


<h2> ERD </h2>
<img src="./public/image/ERD.png" /></div>